from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from events.models import Conference, Location
from django.views.decorators.http import require_http_methods
import json
from django.core.serializers.json import DjangoJSONEncoder


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }


class PresentationListEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Presentation):
            return {
                "id": obj.id,
                "title": obj.title,
                "status": str(obj.status),  # assuming status is a foreign key
                "conference_id": obj.conference.id,  # assuming conference is a foreign key
                # add more fields as needed
            }
        return super().default(obj)


@require_http_methods(["GET", "DELETE", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference_id=conference_id)
        presentations_json = [PresentationListEncoder().default(presentation) for presentation in presentations]
        return JsonResponse(
            {"presentations": presentations_json},
            safe=False,
        )
    if request.method == "DELETE":
        presentations = Presentation.objects.filter(conference_id=conference_id)


class StatusDetailEncoder(ModelEncoder):
    model = Status
    properties = ["name"]

    def default(self, obj):
        if isinstance(obj, Status):
            return {
                "name": obj.name,
                # add more fields as needed
            }
        return super().default(obj)

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
    ]

    def status(self, obj):
        return StatusDetailEncoder().default(obj.status)


@require_http_methods(["GET", "DELETE", "POST"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    if request.method == "DELETE":
        try:
            presentation = Presentation.objects.get(id=presentation_id)
            presentation.delete()
            return JsonResponse({'message': 'Presentation deleted successfully'}, status=200)
        except Presentation.DoesNotExist:
            return JsonResponse({'message': 'Presentation not found'}, status=404)
        
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            presentation = Presentation.objects.get(id=id)
            for key, value in content.items():
                if hasattr(presentation, key):
                    setattr(presentation, key, value)
            presentation.save()
            return JsonResponse(
                {"message": f"Presentation {presentation} updated"},
                status=200,
            )
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"error": f"Presentation {presentation} does not exist"},
                status=404,
            )






# def api_show_presentation(request, id):
#     """
#     Returns the details for the Presentation model specified
#     by the id parameter.

#     This should return a dictionary with the presenter's name,
#     their company name, the presenter's email, the title of
#     the presentation, the synopsis of the presentation, when
#     the presentation record was created, its status name, and
#     a dictionary that has the conference name and its URL

#     {
#         "presenter_name": the name of the presenter,
#         "company_name": the name of the presenter's company,
#         "presenter_email": the email address of the presenter,
#         "title": the title of the presentation,
#         "synopsis": the synopsis for the presentation,
#         "created": the date/time when the record was created,
#         "status": the name of the status for the presentation,
#         "conference": {
#             "name": the name of the conference,
#             "href": the URL to the conference,
#         }
#     }
#     """
#     presentation = Presentation.objects.get(id=id)
#     return JsonResponse(

#         {
#             "presenter_name": presentation.presenter_name,
#             "company_name": presentation.company_name,
#             "presenter_email": presentation.presenter_email,
#             "title": presentation.title,
#             "synopsis": presentation.synopsis,
#             "created": presentation.created,
#             "status": presentation.status,
#             "conference": {
#                 "name": presentation.name,
#                 "href": presentation.get_api_url(),
#             },
#         }
#     )



# def api_list_presentations(request, conference_id):
#     """
#     Lists the presentation titles and the link to the
#     presentation for the specified conference id.

#     Returns a dictionary with a single key "presentations"
#     which is a list of presentation titles and URLS. Each
#     entry in the list is a dictionary that contains the
#     title of the presentation, the name of its status, and
#     the link to the presentation's information.

#     {
#         "presentations": [
#             {
#                 "title": presentation's title,
#                 "status": presentation's status name
#                 "href": URL to the presentation,
#             },
#             ...
#         ]
#     }
#     """
#     presentations = [
#         {
#             "title": p.title,
#             "status": p.status.name,
#             "href": p.get_api_url(),
#         }
#         for p in Presentation.objects.filter(conference=conference_id)
#     ]
#     return JsonResponse({"presentations": presentations})
