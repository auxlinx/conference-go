# import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    base_url = "https://api.pexels.com/v1/search"
    headers = {'Authorization': PEXELS_API_KEY}
    query = f"{city} {state}"
    params = {"query": query, "per_page": 1}
    response = requests.get(base_url, headers=headers, params=params)
    return response.json()


def get_weather_data(city, state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct"
    geo_params = {
        'q': f'{city},{state}',
        'limit': 1,
        'appid': OPEN_WEATHER_API_KEY
    }
    geo_response = requests.get(geo_url, params=geo_params)
    geo_data = geo_response.json()

    lat = geo_data[0]['lat']
    lon = geo_data[0]['lon']

    # Then, get the weather data for the latitude and longitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        'lat': lat,
        'lon': lon,
        'appid': OPEN_WEATHER_API_KEY
    }
    weather_response = requests.get(weather_url, params=weather_params)
    weather_data = weather_response.json()

    # Extract the main temperature and the weather's description
    temperature = weather_data['main']['temp']
    description = weather_data['weather'][0]['description']

    return {
        'temperature': temperature,
        'description': description,
    }



    

# print(get_photo("New York", "NY"))
# print(get_weather_data("New York", "NY"))


#  base_url = "http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit={limit}&appid={OPEN_WEATHER_API_KEY}"
#     headers = {'Authorization': OPEN_WEATHER_API_KEY}
#     params = {
#         'q': f'{city},{state}',
#         'appid': OPEN_WEATHER_API_KEY
#     }
#     response = requests.get(base_url, headers=headers, params=params)
#     return response.json()

    # base_url = "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    # headers = {'Authorization': OPEN_WEATHER_API_KEY}
    # response = requests.get(base_url, headers=headers, params=params)
    # return response.json()